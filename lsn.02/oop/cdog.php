<?php
class CDOG {
    // public $name = 'Bobik';
    // private $name = 'Bobik';
    // protected $name = 'Bobik';

    private $age = 0;
    protected $name = "Bobik";
    private $sex = "Male";
    private $weight = 0;
    private $breed;
    private $energy;
    private $eatAmount;

    public function __construct($name, $age, $sex, $weight, $breed, $energy, $eatAmount) {
        echo 'parent constructor CDOG <br>';
        $this -> name = $name;
        $this -> age = $age;
        $this -> sex = $sex;
        $this -> weight = $weight;
        $this -> breed = $breed;
        $this -> energy = $energy;
        $this -> eatAmount = $eatAmount;

        $this -> showDog();
    }

    private function showDog() {
        echo $this -> name . '<br>';
        echo $this -> age . '<br>';
        echo $this -> sex . '<br>';
        echo $this -> weight . '<br>';
        echo $this -> breed . '<br>';
        echo $this -> energy . '<br>';
        echo $this -> eatAmount . '<br>';
    }
}