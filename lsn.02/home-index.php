<?php

// $title = 'AP.LOC';

// $arr = [1, 2, 3, 'PHP', 'JS', 'CSS'];
// $i = 0;
// for (; $i < count($arr); $i++) {
//     echo $arr[$i] . '<br>';
// }

// PHP 🔥🔥🔥
// http://php.net/manual/ru/ref.array.php
// count
// array_diff
// array_intersect
// array_key_exists
// array_keys
// array_values
// array_merge
// array_rand
// array_reverse
// compact
// extract
// arsort
// asort
// sort
// rsort

// array_combine
// array_search
// array_shift
// array_unique
// array_unshift
// array_flip
// array_pop
// array_push
// in_array
// list

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 
// $arr = ['PHP', ['C++', 'JS', 50]];

// $arr2 = [
//     'colors' => ['blue', 'red', 'yellow'],
//     'languages' => ['PHP', 'JS'],
//     'key' => 'hello'
// ];

// 🔥 echo variables with ""
// echo "Hello {$arr2['languages'][0]}";

// 🔥 start arr from 1
// $first_quarter = [1 => 'Hello', 'World!'];
// echo count($first_quarter);
// var_dump($first_quarter);

// 🔥 $arr2 echo
// echo $arr2['colors'][0];
// echo $arr2['key'];
 
// 🔥🔥🔥 autoindexing array

// $array = array(1, 1, 1, 1, 1, 8 => 1, 4 => 1, 19, 3 => 13);
// $array = [1, 1, 1, 1, 1, 8 => 1, 4 => 1, 19, 3 => 13];
// print_r($array);
// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥  count($array) $arr
// $arrMax = count($arr);
// echo $arrMax;
// var_dump($arrMax);
// echo '<br>' . $arr[1][0];
// echo count($arr, COUNT_RECURSIVE); // 5 $arr => 4


// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_diff($array1, $array2, ...)

// $array1 = array("a" => "green", "red", "blue", "red", ['hello', 'qwe']); //error
// $array2 = array("b" => "green", "yellow", "red", ['hello']); //error

// $array1 = array("a" => "green", "red", "blue", "red");
// $array2 = array("b" => "green", "yellow", "red");

// $result = array_diff($array1, $array2);
// echo '<pre>';
// print_r($result); //blue index 1
// echo '</pre>';


// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_intersect($array1, $array2, ...)

// $array1 = array("a" => "green", "red", "blue");
// $array2 = array("b" => "green", "yellow", "red");
// $result = array_intersect($array1, $array2);
// echo '<pre>';
// print_r($result); //green red
// echo '</pre>';

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_key_exists($key, $array) true/false

// $list = [
//     [
//         "title" => "Samsung",
//         "price" => 200
//     ],
//     [
//         "title" => "Lenovo",
//         "price" => 250
//     ]
// ];

// $result = array_key_exists('title', $list[0]);
// echo $result;

// 🔥
// $search_array = array('first' => 1, 'second' => 4);

// echo isset($search_array['first']); // ??????????? 🔥

// if (array_key_exists('first', $search_array)) {
//     echo "Массив содержит элемент 'first'.";
// } else {
//     echo 'Error';
// }

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_keys($array, $value, strict)

// $array = array(
//     0 => 100, 
//     "color" => "red"
// );
// echo '<pre>';
// print_r(array_keys($array));
// echo '</pre>';
// echo '<br>';

// $array = array("blue", "red", "green", "blue", "blue");
// print_r(array_keys($array, "blue"));
// echo '<br>';

// $array = array(
//     "color" => array("blue", "red", "green"),
//     "size" => array("small", "medium", "large")
// );
// print_r(array_keys($array));
// echo '<br>';
// print_r(array_keys($array['color'], 'red'));

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_values($array)

// $list = [
//     [
//         "title" => "Samsung",
//         "price" => 200
//     ],
//     [
//         "title" => "Lenovo",
//         "price" => 250
//     ]
// ];
// echo '<pre>';
// print_r(array_values($list));
// echo '</pre>';

// $array = array("size" => "XL", "color" => "gold");
// print_r(array_values($array));

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_merge($array1, $array2, ...)

// $array1 = array("color" => "red", 2, 4);
// $array2 = array("a", "b", "color" => "green", "shape" => "trapezoid", 4);
// $result = array_merge($array1, $array2);
// echo '<pre>';
// print_r($result);
// echo '</pre>';

// 🔥 index is changed 1 => 0

// $array1 = array();
// $array2 = array(1 => "data");
// $result = array_merge($array1, $array2);
// echo '<pre>';
// print_r($array2);
// print_r($result);
// echo '</pre>';

// 🔥 index isn't changed if $array1 + $array2

// $array1 = array(0 => 'zero_a', 2 => 'two_a', 3 => 'three_a');
// $array2 = array(1 => 'one_b', 3 => 'three_b', 4 => 'four_b');
// $result = $array1 + $array2;
// // $result = array_merge($array1, $array2);
// echo '<pre>';
// //var_dump($result);
// print_r($result);
// echo '</pre>';

// 🔥 if it isn't array ($variable)

// $beginning = 'foo';
// $end = array(1 => 'bar');
// $result = array_merge((array) $beginning, (array) $end);
// echo '<pre>';
// print_r($result);
// echo '</pre>';

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_rand($array, num)

// $input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
// $rand_keys = array_rand($input, 2);
// echo $input[$rand_keys[0]] . "\n <br>";
// echo $input[$rand_keys[1]] . "\n <br>";

// 🔥 if num is 1

// $rand_keys = array_rand($input, 1);
// echo 'One value: ' . $input[$rand_keys] . "\n";

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_reverse($array, preserve_keys)

// $input = array("php", 4.0, array("green", "red"));
// $reversed = array_reverse($input);
// $preserved = array_reverse($input, true);

// echo '<pre>';
// print_r($input);
// print_r($reversed);
// print_r($preserved);
// echo '</pre>';

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 compact($var1, ...)

// $city = "San Francisco";
// $state = "CA";
// $event = "SIGGRAPH";

// $location_vars = array("city", "state");

// $result = compact("event", "nothing_here", $location_vars);
// echo '<pre>';
// print_r($result);
// echo '</pre>';

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 extract($var1)

// $size = "large";
// $var_array = array(
//     "color" => "blue",
//     "size" => "medium",
//     "shape" => "sphere", 
//     23 
// );
// extract($var_array, EXTR_PREFIX_SAME, "wddx");

// echo "$color, $size, $shape, $wddx_size\n";

// 🔥 bad practice to use extract() $_GET / $_FILES

// var_dump($testfile);
// extract($_FILES, EXTR_SKIP);
// var_dump($testfile);
// var_dump($testfile['tmp_name']);

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 arsort($array, sort_flags) reverse sort

// $fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
// arsort($fruits);
// foreach ($fruits as $key => $val) {
//     echo "$key = $val\n";
// }

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 asort($array, sort_flags) normal sort

// $fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
// asort($fruits);
// foreach ($fruits as $key => $val) {
//     echo "$key = $val\n";
// }

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 sort($array, sort_flags) normal sort

// $fruits = array("lemon", "orange", "banana", "apple");
// sort($fruits);
// foreach ($fruits as $key => $val) {
//     echo "fruits[" . $key . "] = " . $val . "\n";
// }

// 🔥

// $fruits = array(
//     "Orange1", "orange2", "Orange3", "orange20",
// );
// sort($fruits, SORT_NATURAL | SORT_FLAG_CASE);
// foreach ($fruits as $key => $val) {
//     echo "fruits[" . $key . "] = " . $val . "\n";
// }

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 rsort($array, sort_flags) reverse sort

// $fruits = array("lemon", "orange", "banana", "apple");
// rsort($fruits);
// foreach ($fruits as $key => $val) {
//     echo "fruits[" . $key . "] = " . $val . "\n";
// }


// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_combine($keys, $values)

// $a = array('green', 'red', 'yellow');
// $b = array('avocado', 'apple', 'banana');
// $c = array_combine($a, $b);
// echo '<pre>';
// print_r($c);
// echo '</pre>';


// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_search($key, $array, string)

// $array = array(0 => 'blue', 1 => 'red', 2 => 'green', 3 => 'red');

// $key = array_search('green', $array); // $key = 2;
// $key = array_search('red', $array); // $key = 1;

// echo $key;

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_shift($array)

// $stack = array("orange", "banana", "apple", "raspberry");
// $fruit = array_shift($stack);
// echo '<pre>';
// print_r($stack);
// echo '</pre>';

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_unshift($array, ...elements)

// $queue = array("orange", "banana");
// array_unshift($queue, "apple", "raspberry");

// echo '<pre>';
// print_r($queue);
// echo '</pre>';


// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_unique($array, sort_flags)

// $input = array("a" => "green", "red", "b" => "green", "blue", "red");
// $result = array_unique($input);

// echo '<pre>';
// print_r($result);
// echo '</pre>';

// 🔥 

// $input = array(4, "4", "3", 4, 3, "3", 'blue'); // ????????? 🔥 
// $result = array_unique($input);
// echo '<pre>';
// var_dump($result);
// // print_r($result);
// echo '</pre>';

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_flip($array) key <==> value

// $input = array("oranges", "apples", "pears");
// $flipped = array_flip($input);

// echo '<pre>';
// print_r($flipped);
// echo '</pre>';

// 🔥

// $input = array("a" => 1, "b" => 1, "c" => 2);
// $flipped = array_flip($input);

// echo '<pre>';
// print_r($flipped);
// echo '</pre>';

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_pop($array)

// $stack = array("orange", "banana", "apple", "raspberry");
// $fruit = array_pop($stack);

// echo '<pre>';
// print_r($stack);
// echo '</pre>';

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 array_push($array, ...elements)

// $stack = array("orange", "banana");
// array_push($stack, "apple", "raspberry");

// echo '<pre>';
// print_r($stack);
// echo '</pre>';

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 in_array($value, $array, string)

// $os = array("Mac", "NT", "Irix", "Linux");
// if (in_array("Irix", $os)) {
//     echo "Нашел Irix";
// }
// if (in_array("mac", $os)) {
//     echo "Нашел mac";
// }

// 🔥 string = true

// $a = array('1.10', 12.4, 1.13);

// if (in_array('12.4', $a, true)) {
//     echo "'12.4' найдено со строгой проверкой\n";
// }

// if (in_array(1.13, $a, true)) {
//     echo "1.13 найдено со строгой проверкой\n";
// }

// 🔥 $value = array

// $a = array(array('p', 'h'), array('p', 'r'), 'o');

// if (in_array(array('p', 'h'), $a)) {
//     echo "'ph' найдено\n";
// }

// if (in_array(array('f', 'i'), $a)) {
//     echo "'fi' найдено\n";
// }

// if (in_array('o', $a)) {
//     echo "'o' найдено\n";
// }

// 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥 list($var)

// $info = array('кофе', 'коричневый', 'кофеин');

// // Составить список всех переменных
// list($drink, $color, $power) = $info;
// echo "$drink - $color, а $power делает его особенным.\n";

// // Составить список только некоторых из них
// list($drink, , $power) = $info;
// echo "В $drink есть $power.\n";

// // Или пропустить все, кроме третьей
// list(, , $power) = $info;
// echo "Мне нужен $power!\n";

// // list() не работает со строками
// list($bar) = "abcde";
// var_dump($bar); // NULL




