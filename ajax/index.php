<?php
if (!empty($_POST)) {

    if (isset($_POST['submit'])) {
        $mail = 'awesomedruce@gmail.com';
        $site_name = 'andrew paniuk';
        $name = $_POST['name'];
        $title = $_POST['title'];
        $message = "Name: $name \nTitle: $title";
        $page_title = "New title from $site_name";
        mail($mail, $page_title, $message);
        echo 'SENT';
    }

}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
			  crossorigin="anonymous"></script>
</head>

<body>
    <form id="form">
        <label for="name">
            Name
            <input type="text" name="name" id="name">
        </label>
        <label for="title">
            Title
            <input type="text" name="title" id="title">
        </label>
        <input type="submit" value="Send" name="submit">
    </form>
    <div id="time"></div>
    <script>
        function show() {
            $.ajax({
                type: 'POST',
                url: 'mail.php',
                data: $(this).serialize(),
                success: function(html) {
                    let now = new Date(),
                    nowHours = now.getHours(),
                    nowMinutes = now.getMinutes(),
                    nowSeconds = now.getSeconds();
                    $('#time').html(`Server (PHP) time: ${html} || Local (JS) time: ${nowHours}:${nowMinutes}:${nowSeconds}`);
                },
                error: function() {
                    alert('something wrong');
                }
            });
        }
    setInterval(show, 1000);
    </script>
</body>

</html>