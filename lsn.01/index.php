<html>
<head>
<title>Lesson 01</title>
</head>
</html>

<?php

// variables 🔥🔥🔥🔥🔥
$a = 10;
$b = 15;


// if...else 🔥🔥🔥🔥🔥

// if ($a < $b) {
//     echo "<p> $a < $b </p>";
// } else if ($a > $b) {
//     echo "<p> $a > $b </p>";
// } else {
//     echo "<p> $a == $b </p>";
// }

// function sum($a, $b) {
//     return $a + $b;
// }

// echo sum($a, $b)." function sum";


// switch 🔥🔥🔥🔥🔥

// $i = 1;

// switch($i) {
//     case 1:
//         echo 'Zyma';
//         break;
//     case 2:
//         echo 'Vesna';
//         break;
//     case 3:
//         echo 'Lito';
//         break;
//     case 4:
//         echo 'Osin';
//         break;
// }


// while 🔥🔥🔥🔥🔥
// $i = 1;
// while ($i <= 10) {
//     echo $i . "<br>";
//     $i++;
// }


// do...while 🔥🔥🔥🔥🔥
// do {
//     echo $i++ . "<br>";
// } while ($i <= 10);


// array 🔥🔥🔥🔥🔥
$arr = array("PHP", "JS", "C++");

// $arr2 = [23, "Test"];

// 🔥🔥
// var_dump($arr);

// echo "<br>";

// print_r($arr);

// 🔥🔥
// echo "<pre>";
// var_dump($arr);
// echo "</pre>";

// echo "<br>";

// echo "<pre>";
// print_r($arr);
// echo "</pre>";

// 🔥🔥🔥🔥🔥
// $arr3 = [1, 2, 3, "test", ["JS", "PHP", "C++"], 84];

// echo "<pre>";

// var_dump($arr3);

// echo "<br>";

// print_r($arr3);

// echo "</pre>";

// echo $arr3[4][1];

// // 🔥🔥
// $list = [
//     [
//         "title" => "Samsung",
//         "price" => 200
//     ],
//     [
//         "title" => "Lenovo",
//         "price" => 250
//     ]
// ];


// echo "<pre>";

// var_dump($list);

// echo "<br>";

// print_r($list);

// echo "</pre>";

// echo $list[0]["title"] . " - " . $list[0]["price"] . " Rubchikov";

?>

